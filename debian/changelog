dired-du (0.5.2-5) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:59:08 +0900

dired-du (0.5.2-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:51:23 +0900

dired-du (0.5.2-3) unstable; urgency=medium

  * Add patch to fix test expectations (Closes: #1058522)
    Thanks to Jeremy Sowden

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 09 Jan 2024 15:15:50 +0500

dired-du (0.5.2-2) unstable; urgency=medium

  * Build against newer dh_elpa
  * d/control: Migrate to debhelper-compat 13
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/control: Drop emacs25 from Enhances
  * d/copyright: Bump copyright years
  * d/watch: Migrate to version 4

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 10 Dec 2020 19:53:49 +0500

dired-du (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2
  * Migrate to debhelper 12 without d/compat
  * d/copyright: Bump copyright years
  * d/control: Declare Standards-Version 4.4.0 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 09 Jul 2019 09:57:43 +0500

dired-du (0.5.1-1) unstable; urgency=medium

  * New upstream version 0.5.1
  * Migrate to dh 11
  * d/control: Don't need to depend on coreutils
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/control: Declare Standards-Version 4.2.1 (no changes needed)
  * d/control: Remove emacsen-common from Depends
  * d/control: Do not enhance emacs24, only emacs and emacs25
  * d/control: Remove Built-Using field
  * d/control: Add Rules-Requires-Root: no
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 02 Oct 2018 11:58:45 +0500

dired-du (0.5-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Fri, 01 Jun 2018 20:44:46 -0300

dired-du (0.5-1) unstable; urgency=medium

  * Initial release (Closes: #865438)

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 21 Jun 2017 17:26:33 +0500
